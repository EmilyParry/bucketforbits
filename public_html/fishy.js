
// code from max wihlborg on youtube.s
var


canvas,
ctx,
width,
height,
bgpos = 0,
fgpos = 0,
frames = 0,
score = 0,
okbtn,

best = localStorage.getItem("best") || 0;


var currentstate,
states = {
	Splash: 0, Game: 1, Score: 2
};

fish = 
{
    x: 60,
    y: 60,
    frame: 0,
    velocity:0,
    animation: [0,1,2,3,4,5,0], // animation sequence : tells what sprites to go through in which order.
    rotation: 0,
    gravity: 0.25,
    _jump: 4.6,
    

  jump: function()
  {
        this.velocity = -this._jump;
        
  },
  update: function()
  {
      var n = currentstate === states.Splash ? 10: 5;
      this.frame += frames % n === 0? 1:0
      this.frame %= this.animation.length;
      
      
      // makes the fish hover in the water.
      if(currentstate === states.Spalsh)
      {
          this.y = height - 280 + 5*Math.cos(frames/10);
          this.rotation =0; 
      }
      
      else { // game and score state //

			this.velocity += this.gravity;
			this.y += this.velocity;

			// change to the score state when fish touches the ground
			if (this.y >= height - fgpos-15) {
                            if (currentstate === states.Game) {
					currentstate = states.Score;
				}
				this.y = height - fgpos-15;
	
				// sets velocity to jump speed for correct rotation
				this.velocity = this._jump;
			}


			// when fish lack upward momentum increment the rotation
			// angle
			if (this.velocity >= this._jump) {

				this.frame = 1;
				this.rotation = Math.min(Math.PI/2, this.rotation + 0.3);

			} else {

				this.rotation = -0.3;

			}
		}
      
      
      
  },
    
  draw: function(ctx)
  {
      ctx.save();
      ctx.translate(this.x, this.y);
      // for the rotation
      ctx.rotate(this.rotation);
      
      var n = this.animation[this.frame];
      s_fish[n].draw(ctx, -s_fish[n].width/2, -s_fish[n].height/2);
      ctx.restore();
      
      
      
      
  }
    
    
};

enemies = {

	_enemy: [],
	// padding: 80, // TODO: Implement paddle variable

	/**
	 * Empty enemy array
	 */
	reset: function() {
		this._enemy = [];
	},

	/**
	 * Create, push and update all pipes in pipe array
	 */
	update: function() 
        {
		// add new enemy each 100 frames
                if(score <=5)
                {
                    if (frames % 100 === 0) 
                    {
			// calculate y position
			var _y = height - (s_hook.height+fgpos+120+200*Math.random());
			// create and push enemy to array
			this._enemy.push({
				x: 500,
				y: _y,
				width: s_hook.width,
				height: s_hook.height
			});
                    }
                }
                
                else   if((score >5)&&(score <10))
                {
                    if (frames % 80 === 0) 
                    {
			// calculate y position
			var _y = height - (s_hook.height+fgpos+120+200*Math.random());
			// create and push enemy to array
			this._enemy.push({
				x: 500,
				y: _y,
				width: s_hook.width,
				height: s_hook.height
			});
                    }
                }
                
                else   if(score >10)
                {
                    if (frames % 50 === 0) 
                    {
			// calculate y position
			var _y = height - (s_hook.height+fgpos+120+200*Math.random());
			// create and push enemy to array
			this._enemy.push({
				x: 500,
				y: _y,
				width: s_hook.width,
				height: s_hook.height
			});
                    }
                }
                
               
                    
		for (var i = 0, len = this._enemy.length; i < len; i++) 
                {
			var p = this._enemy[i];

			if (i === 0) {

				score += p.x === fish.x ? 1 : 0;

				// collision check, calculates x/y difference and
				// use normal vector length calculation to determine
				// intersection
				var cx  = Math.min(Math.max(fish.x, p.x), p.x+p.width);
				var cy1 = Math.min(Math.max(fish.y, p.y), p.y+p.height);
				var cy2 = Math.min(Math.max(fish.y, p.y+p.height+80), p.y+2*p.height+80);
				// closest difference
				var dx  = fish.x - cx;
				var dy1 = fish.y - cy1;
				var dy2 = fish.y - cy2;
				// vector length
				var d1 = dx*dx + dy1*dy1;
				var d2 = dx*dx + dy2*dy2;
				var r = fish.radius*fish.radius;
				// determine intersection
				if (r > d1 || r > d2) {
					currentstate = states.Score;
				}
			}
			// move enemy and remove if outside of canvas
			p.x -= 2;
			if (p.x < -p.width) {
				this._enemy.splice(i, 1);
				i--;
				len--;
			}
                }
            
	},

	/**
	 * Draw all enemy to canvas context.
	 * 
	 * @param  {CanvasRenderingContext2D} ctx the context used for
	 *                                        drawing
	 */
	draw: function(ctx) 
        {
		for (var i = 0, len = this._enemy.length; i < len; i++) {
			var p = this._enemy[i];
			s_hook.draw(ctx, p.x, p.y);
			s_eel.draw(ctx, p.x, p.y+80+p.height);
		}
	}
};




function onpress(evt) {

	switch (currentstate) {

		// change state and update fish velocity
		case states.Splash:
			currentstate = states.Game;
			fish.jump();
			break;

		// update fish velocity
		case states.Game:
			fish.jump();
			break;

	//change state if event within okbtn bounding box
		case states.Score:
			// get event position
			var mx = evt.offsetX, my = evt.offsetY;

			if (mx === null || my === null) {
				mx = evt.touches[0].clientX;
				my = evt.touches[0].clientY;
			}

			// check if within
			
			break;
	}
}

// *********Game starting here : ************

function main()
{ // loading canvas
    canvas = document.createElement("canvas");
    
    width = 600;
    height = 300;
    
    canvas.style.border = "1px solid #000";
    
    var evt = "mousedown";
    document.addEventListener(evt, onpress);
    
    canvas.width = width;
    canvas.height = height;
    ctx = canvas.getContext("2d");
    
    currentstate = states.Splash;
    
    document.body.appendChild(canvas);
   // creating image object 
   // initate graphics and okbtn
   var img = new Image();
    img.onload = function() 
    {
	initSprites(this);
	
	run();
    }
	img.src = "images/sheet.png";
   // ctx.fillStyle = s__bg.color; 
}

function run()
{
    var loop = function()
    {   
        update();
        render();
       window.requestAnimationFrame(loop, canvas);
        
    };
    window.requestAnimationFrame(loop, canvas);
    
    
}

function update()
{   // updating background to make it move
    frames ++;
    if (currentstate !== states.Score) {
		fgpos = (fgpos - 2) % 14;
	} 
    bgpos = (bgpos - 2)%60;
    
    // makes the enemies update
    
    if (currentstate === states.Game) {
		enemies.update();
	}
    
    fish.update();
    
    
}

function render()
{   
   // ******* Drawing the background****************************    
	
      // fills up the space left where the background doesnt touch
	//s_bg.draw(ctx, 0, height - s_bg.height);
        //s_bg.draw(ctx, s_bg.width, height - s_bg.height);
        //s_bg.draw(ctx, s_bg.width*2, height - s_bg.height);
	//s_eel.draw(ctx,200 ,0);s
        //s_hook.draw(ctx, 100,0);
        ctx.fillStyle = s_bg.color;
        ctx.fillRect(0, 0, width, height);
        
         //***background ********
   
        s_bg.draw(ctx, bgpos, height - s_bg.height );
        s_bg.draw(ctx, bgpos + s_bg.width*2, height - s_bg.height); // scrolling
        s_bg.draw(ctx, bgpos + s_bg.width ,height - s_bg.height); //scrolling.
        
        
        //*** Sprites *****///
        enemies.draw(ctx);
        fish.draw(ctx);
        
       
       
    
    // setting this as the foreground to show the kill zone
    
        s_fg.draw(ctx, -20,height - s_bg.height*0.1);
        s_fg.draw(ctx, 200,height - s_bg.height*0.1);
        s_fg.draw(ctx, 350,height - s_bg.height*0.1);
       // s_fg.draw(ctx, s_fg.width*3,height - s_bg.height*0.1);
        //s_fg.draw(ctx,s_fg.width*4,height - s_bg.height*0.1);
    
    var width2 = width/2; // center of canvas

	if (currentstate === states.Splash) {
		// draw splash text and sprite to canvas
		
		s_text.GetReady.draw(ctx, width2 - s_text.GetReady.width/2, height-380);
	}
	if (currentstate === states.Score) {
		// draw gameover text and score board
		s_text.GameOver.draw(ctx, width2 - s_text.GameOver.width/2, height-400);
		
		s_numberS.draw(ctx, width2-47, height-304, score, null, 10);
		s_numberS.draw(ctx, width2-47, height-262, best, null, 10);

	} else {
		// draw score to top of canvas
		s_numberB.draw(ctx, null, 20, score, width2);

	}
    
    
}

//restart the game if reaching the edge


main();