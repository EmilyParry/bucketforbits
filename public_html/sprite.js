var 

s_fish,
s_bg,
s_fg,
s_eel,
s_hook,
s_score,
s_text;

// Code taken from max Wihlborg
function Sprite(img, x, y, width, height) {
	this.img = img;
	this.x = x*2;
	this.y = y*2;
	this.width = width*2;
	this.height = height*2;
};
Sprite.prototype.draw = function(ctx, x, y) {
	ctx.drawImage(this.img, this.x, this.y, this.width, this.height,
		x, y, this.width, this.height);
};

function initSprites(img) {

	s_fish = 
         [
		new Sprite(img, 2, 115, 18, 39),
                new Sprite(img, 20, 115, 18, 39),
                new Sprite(img, 36, 115, 18, 38), 
                new Sprite(img, 54, 115, 17, 38),
                new Sprite(img, 70 , 115, 17, 38),
                new Sprite(img, 88, 115, 17, 39)
         ],	
	
        s_text = 
        {

		GameOver:   new Sprite(img, 59, 136, 94, 19),
		GetReady:   new Sprite(img, 59, 155, 87, 22)
	};
        
	
        s_bg = new Sprite(img, 0, 3, 135, 105);
        s_bg.color = "#5BF9FF";
	s_fg = new Sprite(img, 145, 3, 100, 60);
	
	s_eel = new Sprite(img, 235, 0,40, 200);
	s_hook = new Sprite(img, 275, 0,30, 220);
	
	s_numberS = new Sprite(img, 0, 184, 6, 7);
	s_numberB = new Sprite(img, 0, 195, 7, 10);
        
        
        
}



