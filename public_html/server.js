var http = require('http'),
	io = require('socket.io'),
	fs = require('fs');

server = http.createServer();
server.listen(49800);
var socket = io.listen(server);

socket.on('connection', function(client)
{
	client.on('life lost', function(data)
	{
		fs.appendFile('lives-lostclass.txt', '('
		+ data.left.toFixed(0) + ', ' + data.top.toFixed(0) + ')', 
		function(err)
		{
			if(err) throw err;
		});
	});

	client.on('chat', function(data)
	{
		fs.appendFile('chat.txt', data.chat,
		function(err)
		{
			if(err) throw err;
		});
	});
});